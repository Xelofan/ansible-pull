# Usage

## fedora-server.yml:

```
sudo dnf in git ansible -y \
&& ansible-pull -U https://gitlab.com/xelofan/ansible-pull fedora-server.yml --ask-become-pass
```

## fedora-desktop.yml:

```
sudo dnf in git ansible -y \
&& ansible-galaxy install petermosmans.customize-gnome \
&& ansible-pull -U https://gitlab.com/xelofan/ansible-pull fedora-desktop.yml --ask-become-pass
```